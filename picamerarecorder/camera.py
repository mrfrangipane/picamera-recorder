from time import sleep

import picamera

class Camera:
    def __init__(self):
        self.camera = picamera.PiCamera()

        try:
            self.camera.stop_recording()
            self.camera.stop_preview()
            self.camera.start_preview()
        except Exception as e:
            print(e)

        self.camera.resolution = (640, 480)
        self.camera.framerate = 15
        self.is_rec = False

    def video_feed(self):
        self.camera.capture('test.jpg', use_video_port=True)

        with open('test.jpg', 'rb') as image:
            return (
                b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + image.read() + b'\r\n'
            )

    def start_rec(self):
        if not self.is_rec:
            self.camera.start_recording('rec.h264')
            self.is_rec = True
            return 'Record started'
        else:
            return 'Already recording'

    def stop_rec(self):
        if self.is_rec:
            self.camera.stop_recording()
            self.is_rec = False
            return 'Record stopped'
        else:
            return 'Not recording'

    def set_low_light(self, low_light):
        if low_light:
            try:
                self.camera.stop_recording()
                self.camera.stop_preview()
                self.camera.start_preview()
            except Exception as e:
                print(e)

            self.camera.iso = 1200
            sleep(2)
            self.camera.exposure_mode = 'off'
            g = self.camera.awb_gains
            self.camera.awb_mode = 'off'
            self.camera.awb_gains = g
            shutter_speed_order = int((1 / self.camera.framerate) * 1000000)
            self.camera.shutter_speed = shutter_speed_order
            return {
                "iso": self.camera.iso,
                "requested_shutter_speed": shutter_speed_order,
                "actual_shutter_speed": self.camera.shutter_speed
            }

    def __del__(self):
        self.is_rec = False
        self.camera.stop_preview()
