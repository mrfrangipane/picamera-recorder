from flask import Flask, render_template, Response
from camera import Camera

app = Flask(__name__)
camera = Camera()

#
## https://picamera.readthedocs.io/en/release-1.13/recipes2.html#web-streaming
#

@app.route('/')
def index():
    return render_template('app.html')


def gen():
    while True:
        yield camera.video_feed()


@app.route('/video_feed')
def video_feed():
    return Response(
        gen(),
        mimetype='multipart/x-mixed-replace; boundary=frame'
    )


@app.route('/start_rec', methods=['POST'])
def start_rec():
    return {"response": camera.start_rec()}


@app.route('/stop_rec', methods=['POST'])
def stop_rec():
    return {"response": camera.stop_rec()}


@app.route('/low_light_on', methods=['POST'])
def low_light_on():
    return {"response": camera.set_low_light(True)}
