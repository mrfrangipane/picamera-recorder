import subprocess
from time import sleep
from datetime import datetime
from picamera import PiCamera


FILENAME = 'picamera'
TIME_SECONDS = 60 * 60 * 2
ISO = 800
FRAMERATE = 15
RESOLUTION = 640, 480
AUTO = True

print("PI CAMERA RECORDER")
print("Adjusting...")
print(datetime.now())

# setup a camera
camera = PiCamera()
camera.resolution = RESOLUTION
camera.framerate = FRAMERATE

if not AUTO:
    camera.iso = ISO

sleep(2)

camera.exposure_mode = 'off'
g = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = g

if AUTO:
    camera.shutter_speed = camera.exposure_speed
    print(f"Shutter speed is {camera.shutter_speed}")
else:
    shutter_speed_order = int((1 / FRAMERATE) * 1000000)
    camera.shutter_speed = shutter_speed_order
    print(f"Shutter speed is {camera.shutter_speed} (order was {shutter_speed_order})")

# record a video
print("Recording...")
print(datetime.now())
camera.start_preview()
camera.start_recording(FILENAME + '.h264')
camera.wait_recording(TIME_SECONDS)
camera.stop_recording()
camera.stop_preview()

# reencode
print("Re encoding...")
print(datetime.now())
subprocess.run(['ffmpeg', '-y', '-hide_banner', '-loglevel', 'error', '-r', str(FRAMERATE), '-i', FILENAME + '.h264', FILENAME + '.mkv'])

print('Done')
print(datetime.now())
