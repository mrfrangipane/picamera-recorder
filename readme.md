# PiCamera Recorder

A basic web server allowing to record PiCamera to local files

## Install service

````
sudo cp picamera-recorder.service /lib/systemd/system
````

````
sudo systemctl enable picamera-recorder.service 
````

## Install auto wifi hotspot 

Add this at the end of `/etc/rc.local`, BEFORE the line "exit 0"

````
printf "Waiting for AP connection (10s)..."
sleep 10
if [ "$(iwconfig wlan0 | grep -c show)" = "0" ]; then
  printf "Not associated to show AP, starting local AP"
  sudo /home/cctv/picamera-recorder/start-access-point.sh
else
  printf "Associated to show AP, no action taken"
fi

# sudo service picamera-recorder start ??

exit 0
````

## Test run

````
cd picamerarecorder
source ../update-and-run.sh
````

## Reencode to proper video file

````
ffmpeg -y -hide_banner -loglevel error -r <framerate> -i <input.h264> <output.mkv>
````
````
ffmpeg -y -hide_banner -loglevel error -r 25 -i rec.h264 output.mkv
````